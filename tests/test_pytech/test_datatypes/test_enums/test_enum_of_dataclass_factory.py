from dataclasses import dataclass

import pytest

from pytech.datatypes.enums import enum_of_dataclass_factory


@pytest.fixture
def example_dataclass():
    """
    Fixture that provides a simple dataclass
    :return: the dataclass
    """

    @dataclass(frozen=True)
    class ExampleDataclass:
        description: str

    return ExampleDataclass


@pytest.fixture
def another_example_dataclass():
    """
    Fixture that provides a simple dataclass
    :return: the dataclass
    """

    @dataclass(frozen=True)
    class AnotherExampleDataclass:
        description: str

    return AnotherExampleDataclass


@pytest.fixture
def enum_of_example_dataclass(example_dataclass):
    """
    Fixture that provides an EnumOfDataclass object built on the example dataclass
    :return: the EnumOfDataclass class
    """

    return enum_of_dataclass_factory(example_dataclass)


@pytest.mark.parametrize(
    "wrong_data", [1, 1.0, "ciao", {}, [], tuple()]
)
def test__raise_value_error_if_not_dataclass(
        enum_of_example_dataclass,
        wrong_data
):
    with pytest.raises(ValueError):
        class ExampleEnum(enum_of_example_dataclass):
            DATA = wrong_data


def test__raise_value_error_if_a_different_dataclass(
        enum_of_example_dataclass,
        another_example_dataclass
):
    with pytest.raises(ValueError):
        class ExampleEnum(enum_of_example_dataclass):
            DATA = another_example_dataclass(
                description="A generic text"
            )


def test__enum_of_dataclass_factory_allows_the_correct_datatype(
        enum_of_example_dataclass,
        example_dataclass
):
    class ExampleEnum(enum_of_example_dataclass):
        DATA = example_dataclass(
            description="A generic text"
        )

    assert ExampleEnum
