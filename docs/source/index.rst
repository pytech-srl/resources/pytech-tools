==============================================
Welcome to PyTech Rules' documentation!
==============================================

********
Modules:
********

* :ref:`pytech.datatypes`
* :ref:`pytech.tools`


.. _pytech.datatypes:

pytech.datatypes
====

.. toctree::
   :maxdepth: 3
   :caption: pytech.datatypes

   pytech.datatypes


.. _pytech.tools:

pytech.tools
====

.. toctree::
   :maxdepth: 3
   :caption: pytech.tools

   pytech.tools


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


|DocumentationVersion|